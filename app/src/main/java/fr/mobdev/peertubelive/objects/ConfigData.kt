/**
 *  Copyright (C) 2021 Anthony Chomienne
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>
 */

package fr.mobdev.peertubelive.objects;

import android.os.Parcel
import android.os.Parcelable

class ConfigData(val liveEnabled: Boolean, val saveReplayEnabled: Boolean) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeByte(if (liveEnabled) 1 else 0)
        dest.writeByte(if (saveReplayEnabled) 1 else 0)
    }

    companion object CREATOR : Parcelable.Creator<StreamData> {
        override fun createFromParcel(parcel: Parcel): StreamData {
            return StreamData(parcel)
        }

        override fun newArray(size: Int): Array<StreamData?> {
            return arrayOfNulls(size)
        }
    }
}
